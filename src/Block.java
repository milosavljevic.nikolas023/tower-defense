import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;


@SuppressWarnings("serial")
public class Block extends Rectangle{
	
	public static final int SIZE = 64;
	
	public static final int grassID = 0;
	public static final int roadID = 1; 
	public static final int roadUpperID = 2; 
	public static final int roadLowerID = 3;
	public static final int roadLeftID = 4;
	public static final int roadLRightID = 5;
	public static final int roadUpperLeftID = 6;
	public static final int roadUpperRightID = 7;
	public static final int roadLowerLeftID = 8;
	public static final int roadLowerRightID = 9;
	public static final int roadInnerUpperLeftID = 10;
	public static final int roadInnerUpperRightID = 11;
	public static final int roadInnerLowerLeftID = 12;
	public static final int roadInnerLowerRightID = 13;
	public static final int riverID = 14;

	private int id;
	private Tower tower;
	
	public void init(int x, int y, int id){
		setBounds(x, y, SIZE, SIZE);
		this.id = id;
		tower = null;
	}
	
	public void setID(int id){
		this.id = id;
	}
	
	public int getID(){
		return id;
	}
	
	public void spawnTower(){
		tower = new Tower(this);
		tower.inGame = true;
	}
	
	public void removeTower(){
		tower = null;
	}
	
	public boolean hasTower(){
		if(tower != null)
			return true;
		else 
			return false;
	}
	
	public Tower getTower(){
		return tower;
	}
	
	public void drawBlock(Graphics g){
		
		Graphics2D g2d = (Graphics2D)g;
		
		g.drawImage(Assets.groundTiles[id], x, y, null);
		
		if(this.contains(EventManager.gameScreenMouse)){
			if(tower == null){
				g2d.setColor(new Color(225, 255, 255, 50));
				g2d.fillRect(x, y, SIZE, SIZE);
			}
			else if(tower != null){
				//tower.showRange();
			}
		}
	}
	
}
