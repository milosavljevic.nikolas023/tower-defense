import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class MenuPanel extends JPanel{

	public static final int WIDTH = 128;
	public static final int HEIGHT = 640;
	public static Dimension size = new Dimension(WIDTH, HEIGHT);
	public static Rectangle menuPanelArea = new Rectangle(StorePanel.WIDTH + GameScreen.WIDTH, 0, WIDTH, HEIGHT);
	
	public Rectangle[] buttons;
	public int hGap = 32;
	public int vGap = 32;
	public Dimension buttonSize = new Dimension(64, 48);
	
	private Screen screen;
	
	public MenuPanel(Screen screen){
		this.screen = screen;
		setMinimumSize(size);
		setMaximumSize(size);
		setPreferredSize(size);
		setBackground(new Color(40, 2, 2));
		setLayout(new BorderLayout());
		
		initMenu();
	}
	
	private void initMenu(){
		JLabel img = new JLabel(new ImageIcon(Assets.menuBackground));
		add(img, BorderLayout.CENTER);
		
		buttons = new Rectangle[4];
		buttons[0] = new Rectangle(25, 186, 78, 154);
		buttons[1] = new Rectangle(hGap, 388, buttonSize.width, buttonSize.height);
		buttons[2] = new Rectangle(hGap, 388 + (vGap + buttonSize.height) , buttonSize.width, buttonSize.height);
		buttons[3] = new Rectangle(hGap, 388 + 2*(vGap + buttonSize.height), buttonSize.width, buttonSize.height);
	}
	
	public void showInfo(){
		JOptionPane.showMessageDialog(screen, "Igrica napravljena za maturski 2015.", "Info.", JOptionPane.INFORMATION_MESSAGE);
	}
	
	public void clicked(int mouseButton, Point p){
		if(buttons[1].contains(p))
			showInfo();
		if(buttons[2].contains(p)){
			if(StateManager.gameState)
				StateManager.pauseGame();
			else if(StateManager.pauseState)
				StateManager.resumeGame();
		}
		if(buttons[3].contains(p))
			System.exit(0);
	}
	
	public void paint(Graphics g){
		super.paint(g);

		for(int i = 0; i < buttons.length; i++){
			if(buttons[i].contains(EventManager.menuPanelMouse)){
				g.setColor(new Color(255, 255, 255, 50));
				g.fill3DRect(buttons[i].x, buttons[i].y, buttons[i].width, buttons[i].height, true);
			}
		}
		
		g.setColor(new Color(255, 255, 255, 200));
		g.setFont(new Font("Courier New", Font.BOLD, 15));
		g.drawString(LevelManager.player_hp + "", 67, 80);
		g.drawString(LevelManager.player_money + "", 67, 130);
	}
	
}
