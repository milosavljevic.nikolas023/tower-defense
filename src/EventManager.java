import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;


public class EventManager implements MouseListener, MouseMotionListener, KeyListener{

	public static Point gameScreenMouse = new Point();
	public static Point storePanelMouse = new Point();
	public static Point menuPanelMouse = new Point();

	private Screen screen;
	
	public EventManager(Screen screen){
		this.screen = screen;
	}
	
	@Override
	public void mouseDragged(MouseEvent e) {
		gameScreenMouse.x = e.getX() - StorePanel.size.width;
		gameScreenMouse.y = e.getY();
		
		storePanelMouse.x = e.getX();
		storePanelMouse.y = e.getY();
		
		menuPanelMouse.x = e.getX() + (StorePanel.size.width + GameScreen.size.width);
		menuPanelMouse.y = e.getY();
	}
	
	@Override
	public void mouseMoved(MouseEvent e) {
		gameScreenMouse.x = e.getX() - StorePanel.size.width;
		gameScreenMouse.y = e.getY();
		
		storePanelMouse.x = e.getX();
		storePanelMouse.y = e.getY();
		
		menuPanelMouse.x = e.getX() - (StorePanel.size.width + GameScreen.size.width);
		menuPanelMouse.y = e.getY();
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		if(!StateManager.preGame){
			//========= Store Panel ===============================================
			
			if(StorePanel.storePanelArea.contains(e.getPoint())){
				screen.getStorePanel().clicked(e.getButton(), storePanelMouse);
			}
			
			//======== Game Screen ================================================
			if(StateManager.gameState){
				if(GameScreen.gameScreenArea.contains(e.getPoint())){
					screen.getGameScreen().clicked(e.getButton(), gameScreenMouse);
				}
			}
			
			//========= Menu Panel ================================================
			
			if(MenuPanel.menuPanelArea.contains(e.getPoint())){
				screen.getMenuPanel().clicked(e.getButton(), menuPanelMouse);
			}
		}
		else if(StateManager.preGame){
			StateManager.startGame();
		}
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

}
