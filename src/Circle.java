
public class Circle {

	public int r;
	public int x, y;
	
	public Circle(int x, int y, int r){
		this.x = x;
		this.y = y;
		this.r = r;
	}
	
	public boolean intersects(Circle c){
		
		double d = Math.sqrt(Math.pow((double)(this.y - c.y), 2.0D) + Math.pow((double)(this.x - c.x), 2.0D));
		
		if(d <= (this.r + c.r))
			return true;
		
		return false;
	}
}
