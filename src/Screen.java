import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class Screen extends JPanel implements Runnable{

	private Thread t;
	private EventManager listener;
	public GameScreen gameScreen;
	public MenuPanel menuPanel;
	public StorePanel storePanel;
	private LevelManager levelManager;
	
	private boolean firstTick = true;
	
	public boolean holdingTower = false;
	public boolean sellMode = false;
	
	private int currentlvl = 1;
	private int maxlvl = 3;
	
	public Screen(){
		Assets.loadImages();
		t = new Thread(this);
		requestFocus();
		levelManager = new LevelManager();
		listener = new EventManager(this);
		
		addMouseListener(listener);
		addMouseMotionListener(listener);
		addKeyListener(listener);
		
		storePanel = new StorePanel(this);
		gameScreen = new GameScreen(this, levelManager);
		menuPanel = new MenuPanel(this);
		
		setBackground(Color.BLACK);
		setLayout(new BorderLayout(0, 0)); //razmak izmedju komponenti je 0
		add(storePanel, BorderLayout.WEST);
		add(menuPanel, BorderLayout.EAST);
		add(gameScreen, BorderLayout.CENTER);
		
		t.start();
	}
	
	private void init(){
		levelManager.initLevel(currentlvl);
		firstTick = false;
	}
	
	private void update(){
		if(StateManager.gameState)
			levelManager.updateLevel();
	}
	
	public void paint(Graphics g){
		super.paint(g);
		if(StateManager.preGame){
			drawStartScreen(g);
		}
		if(StateManager.pauseState){
			drawPauseScreen(g);
		}
		if(StateManager.gameOverState){
			drawGameOverScreen(g);
		}
	}
	
	public void drawStartScreen(Graphics g){
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, getWidth(), getHeight());
		g.setColor(new Color(255, 255, 255, 200));
		g.setFont(new Font("Courier New", Font.BOLD, 35));
		g.drawString("Kliknite da pokrenete igru.", getWidth()/2 - 300, getHeight()/2 - 20);
	}
	
	public void drawPauseScreen(Graphics g){
		g.setColor(new Color(0, 0, 0, 200));
		g.fillRect(0,  0,  getWidth(), getHeight());
		g.setColor(new Color(255, 255, 255, 200));
		g.setFont(new Font("Courier New", Font.BOLD, 50));
		g.drawString("Igra je pauzirana.", getWidth()/2 - 225, getHeight()/2 - 20);
	}
	
	public void drawGameOverScreen(Graphics g){
		g.setColor(Color.BLACK);
		g.fillRect(0,  0,  getWidth(), getHeight());
		g.setColor(new Color(255, 255, 255, 200));
		g.setFont(new Font("Courier New", Font.BOLD, 50));
		g.drawString("Kraj igre.", getWidth()/2 - 225, getHeight()/2 - 20);
	}
	
	public void run(){
		long lastTime = System.nanoTime();
		int FPS = 60;
		final double ns = 1000000000.0 / FPS;
		double delta = 0;
		while(true){
			long now = System.nanoTime();
			delta+= (now - lastTime) /ns;
			while(delta>=1){
				//Kod za izvrsavanje ide ispod
				//_____________________________
				if(firstTick){
					init();
					continue;
				}
				if(levelManager.checkIfLevelDone()){
					if(currentlvl == maxlvl){
						StateManager.gameOver();
					}
					currentlvl++;
					init();
					continue;
				}
				update();
				repaint();
				//___________________________
				delta--;
			}
			lastTime=now;
		}
	}
	
	public StorePanel getStorePanel(){
		return storePanel;
	}
	
	public GameScreen getGameScreen(){
		return gameScreen;
	}
	
	public MenuPanel getMenuPanel(){
		return menuPanel;
	}
	
}
