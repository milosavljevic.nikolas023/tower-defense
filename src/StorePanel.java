import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class StorePanel extends JPanel{

	public static final int WIDTH = 128;
	public static final int HEIGHT = 640;
	public static final Dimension size = new Dimension(WIDTH, HEIGHT);
	public static final Rectangle storePanelArea = new Rectangle(0, 0, WIDTH, HEIGHT);
	
	public static final int STORE_SLOTS = 6;
	public Rectangle[] storeSlots;
	public int vGap = 20;
	public int hGap = 32;
	
	private Screen screen;
	//private boolean firstTime = true;
	
	public StorePanel(Screen screen){
		this.screen = screen;
		setMinimumSize(size);
		setMaximumSize(size);
		setPreferredSize(size);
		setLayout(new BorderLayout());
		
		initStore();
	}
	
	public void initStore(){
		
		storeSlots = new Rectangle[STORE_SLOTS];
		add(new JLabel(new ImageIcon(Assets.shopBackground)), BorderLayout.CENTER);
		
		for(int i = 0; i < STORE_SLOTS; i++){
			if(i == STORE_SLOTS - 1)
				storeSlots[i] = new Rectangle(hGap, HEIGHT - (vGap + Block.SIZE), Block.SIZE, Block.SIZE);
			else
				storeSlots[i] = new Rectangle(hGap, vGap + i*(vGap + Block.SIZE), Block.SIZE, Block.SIZE);
		}
		
	}
	
	public void clicked(int mouseButton, Point p){
		
		if(storeSlots[0].contains(p)){
			screen.holdingTower = true;
			screen.sellMode = false;
		}
		else if(storeSlots[STORE_SLOTS - 1].contains(p)){
			screen.holdingTower = false;
			screen.sellMode = true;
		}
		
		
	}
	
	public void paint(Graphics g){
		super.paint(g);
		
		for(int i = 0; i < STORE_SLOTS; i++){
			if(storeSlots[i].contains(EventManager.storePanelMouse)){
				g.setColor(new Color(255, 255, 255, 100));
				g.fill3DRect(storeSlots[i].x, storeSlots[i].y, storeSlots[i].width, storeSlots[i].height, true);
			}
		}
		g.drawImage(Assets.towerIcon, storeSlots[0].x, storeSlots[0].y, null);
		if(screen.holdingTower){
			g.setColor(new Color(255, 255, 255, 100));
			g.fill3DRect(storeSlots[0].x, storeSlots[0].y, storeSlots[0].width, storeSlots[0].height, true);
		}

		g.drawImage(Assets.dollarIcon, storeSlots[STORE_SLOTS - 1].x, storeSlots[STORE_SLOTS - 1].y, null);
		if(screen.sellMode){
			g.setColor(new Color(255, 255, 255, 100));
			g.fill3DRect(storeSlots[STORE_SLOTS - 1].x, storeSlots[STORE_SLOTS - 1].y, storeSlots[STORE_SLOTS - 1].width, storeSlots[STORE_SLOTS - 1].height, true);
		}
	}
	
}
